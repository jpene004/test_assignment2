# README #

### What is this shell script for? ###
wordpressAutomation.sh is for the fully automated configuration of a linux server and installation of word press system.

### Prerequisites ###
To get the script running properly, a clean ubuntu system is necessary.
Our project choose to use AWS(Amazon Web Service) with Ubuntu Server 16.04 LTS (HVM) 
Detailed Steps of creating AWS EC2 instance please refer:
https://aws.amazon.com/getting-started/tutorials/launch-a-virtual-machine/

### How to get the script file? ###
Use "git clone https://bitbucket.org/1583831702group2/devops.git" command to clone this repository to your local system

Attention: Recommend running git on Linux system, because you need to run this script on linux finally.
If you run git command on windows, additional line ending character (CRLF) might be added into script and cause the script cann't execute normally. 

### How to run this script? ###
Normally /bin already included in $PATH, so this script can be executed anywhere.

### How to access web server###
After running wordpressAutomation.sh, wordpress should be insalled on your server successfully.
Default User name and password are set by shell script automatically. 
Default_admin_user_name ="admin"
Default_admin_user_password = "admin"
Default_admin_email = "no@spam.org"

You can access web server from remote web browser by inputting web server's public ip address.

### Test and Verify ###
This script can be executed on any clear ubuntu 16.04 system, besides AWS EC2, it is verified on google cloud as well.

### Who do I talk to? ###
Please contact group2 members if any question or issue:
Charmaine Fajardo <charmaine.j.f@gmail.com> , Charmane Sangcap <scharmane@hotmail.com>
Jordan Pene <jpene03124010@gmail.com> , Elisa Liu <mommyofqq@gmail.com>


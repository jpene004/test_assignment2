#!/bin/bash
# the following code is modified from https://peteris.rocks/blog/unattended-installation-of-wordpress-on-ubuntu-server/

#update before installing software
sudo apt-get update
sudo apt-get install curl

#set up variables to be used during installation
WP_DOMAIN="$(curl ifconfig.me)"
WP_ADMIN_USERNAME="admin"
WP_ADMIN_PASSWORD="admin"
WP_ADMIN_EMAIL="no@spam.org"
WP_DB_NAME="wordpress"
WP_DB_USERNAME="wordpress"
WP_DB_PASSWORD="wordpress"
WP_PATH="/var/www/wordpress"
MYSQL_ROOT_PASSWORD="root"

#configure MySQL – this creates a user and database for wordpress using the variables
mysql -u root -p$MYSQL_ROOT_PASSWORD <<EOF
CREATE USER '$WP_DB_USERNAME'@'localhost' IDENTIFIED BY '$WP_DB_PASSWORD';
CREATE DATABASE $WP_DB_NAME;
GRANT ALL ON $WP_DB_NAME.* TO '$WP_DB_USERNAME'@'localhost';
EOF

#Configure Nginx. Changed server_name to regular expression so it will accept all domain names 
sudo mkdir -p $WP_PATH/public $WP_PATH/logs
sudo tee /etc/nginx/sites-available/$WP_DOMAIN <<EOF
server {
  listen 80;
  server_name ~^(.+)$;

  root $WP_PATH/public;
  index index.php;

  access_log $WP_PATH/logs/access.log;
  error_log $WP_PATH/logs/error.log;

  location / {
    try_files \$uri \$uri/ /index.php?\$args;
  }

  location ~ \.php\$ {
    include snippets/fastcgi-php.conf;
    fastcgi_pass unix:/run/php/php7.0-fpm.sock;
  }
}
EOF
sudo ln -s /etc/nginx/sites-available/$WP_DOMAIN /etc/nginx/sites-enabled/$WP_DOMAIN
sudo rm -f /etc/nginx/sites-available/default 
sudo systemctl restart nginx
sudo rm -rf $WP_PATH/public/ # !!!
sudo mkdir -p $WP_PATH/public/
sudo chown -R $USER $WP_PATH/public/
cd $WP_PATH/public/

#Install wordpress - fetches the latest version of the wordpress source code. The wp-config file is modified to enable dynamic urls. 
wget https://wordpress.org/latest.tar.gz
tar xf latest.tar.gz --strip-components=1
rm latest.tar.gz

mv wp-config-sample.php wp-config.php
sed -i s/database_name_here/$WP_DB_NAME/ wp-config.php
sed -i s/username_here/$WP_DB_USERNAME/ wp-config.php
sed -i s/password_here/$WP_DB_PASSWORD/ wp-config.php
sed -i s/require_once\(ABSPATH\ .\ \'wp-settings\.php\'\)\;// wp-config.php
tee -a wp-config.php <<EOF

#Enables dynamic urls
/* Enable dynamic urls */
\$currenthost = "http://".\$_SERVER['HTTP_HOST'];
\$currentpath = preg_replace('@/+\$@','',dirname(\$_SERVER['SCRIPT_NAME']));
\$currentpath = preg_replace('/\/wp.+/','',\$currentpath);
define('WP_HOME',\$currenthost.\$currentpath);
define('WP_SITEURL',\$currenthost.\$currentpath);
define('WP_CONTENT_URL', \$currenthost.\$currentpath.'/wp-content');
define('WP_PLUGIN_URL', \$currenthost.\$currentpath.'/wp-content/plugins');
define('DOMAIN_CURRENT_SITE', \$currenthost.\$currentpath );
@define('ADMIN_COOKIE_PATH', './');

require_once(ABSPATH . 'wp-settings.php');
define('FS_METHOD', 'direct');
EOF

#Set up the username and password using the variables

sudo chown -R www-data:www-data $WP_PATH/public/

curl "http://localhost/wp-admin/install.php?step=2" \
  --data-urlencode "weblog_title=158383 Group 2"\
  --data-urlencode "user_name=$WP_ADMIN_USERNAME" \
  --data-urlencode "admin_email=$WP_ADMIN_EMAIL" \
  --data-urlencode "admin_password=$WP_ADMIN_PASSWORD" \
  --data-urlencode "admin_password2=$WP_ADMIN_PASSWORD" \
  --data-urlencode "pw_weak=1"

#advises the admin site address, username and password
echo "If you want to access the admin page for this site go to http://$WP_DOMAIN/wp-admin"
echo "Default username: $WP_ADMIN_USERNAME"
echo "Default password: $WP_ADMIN_PASSWORD"